//
//  StarViewController.m
//  com.csym.BlueToothDemo
//
//  Created by CSYM_iOS_006 on 16/8/12.
//  Copyright © 2016年 深圳市创世易明科技有限公司. All rights reserved.
//

#import "StarViewController.h"
#import "ViewController.h"
@interface StarViewController ()

@end
#define WIDTH [UIScreen mainScreen].bounds.size.width
#define HEIGHT [UIScreen mainScreen].bounds.size.height

@implementation StarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    UILabel *lab = [[UILabel alloc] initWithFrame:CGRectMake(0, (HEIGHT - 50)/2.0, WIDTH, 50)];
    [self.view addSubview:lab];
    lab.font = [UIFont systemFontOfSize:50];
    lab.text = @"点击进行录制";
    lab.textColor = [UIColor blackColor];
    lab.textAlignment = NSTextAlignmentCenter;
    
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    ViewController *cn = [[ViewController alloc] init];
    [self presentViewController:cn animated:YES completion:nil];
}
@end
