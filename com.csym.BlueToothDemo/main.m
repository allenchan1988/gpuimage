//
//  main.m
//  com.csym.BlueToothDemo
//
//  Created by CSYM_iOS_006 on 16/8/11.
//  Copyright © 2016年 深圳市创世易明科技有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
