//
//  MBProgressHUD+Extension.h
//  MBProgressHUD-Demo
//
//  Created by Mopon on 16/5/9.
//  Copyright © 2016年 Mopon. All rights reserved.
//

#import "MBProgressHUD.h"

@interface MBProgressHUD (Extension)

+ (void)showSuccess:(NSString *)success toView:(UIView *)view;
+ (void)showError:(NSString *)error toView:(UIView *)view;

+ (MBProgressHUD *)showMessage:(NSString *)message toView:(UIView *)view;

/**
 *  显示文字并且自动隐藏的HUD
 */
+(void)showAutoMessage:(NSString *)message;

+ (void)showSuccess:(NSString *)success;
+ (void)showError:(NSString *)error;

+ (MBProgressHUD *)showMessage:(NSString *)message;

+ (void)hideHUDForView:(UIView *)view;
+ (void)hideHUD;

+(MBProgressHUD *)showInview:(UIView *)view title:(NSString *)title;

@end
