//
//  ViewController.m
//  com.csym.BlueToothDemo
//
//  Created by CSYM_iOS_006 on 16/8/11.
//  Copyright © 2016年 深圳市创世易明科技有限公司. All rights reserved.
//

#import "ViewController.h"
#import <AVFoundation/AVFoundation.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <MBProgressHUD.h>
#import "MBProgressHUD+Extension.h"
//#import <GPUImageLocalBinaryPatternFilter.h>
//引入头文件
#import <GPUImage.h>
@interface ViewController ()
{
    GPUImageVideoCamera *videoCamera;
    GPUImageOutput<GPUImageInput> *filter;
    GPUImageMovieWriter *movieWriter;
}
@property (nonatomic,assign) BOOL coding;
@property (nonatomic,strong)NSString *pathToMovie;
@property (nonatomic,strong)NSURL *movieURL;
@property (nonatomic,strong)UISlider *slider;
@property (nonatomic,strong)UIButton *changeCamre;//切换摄像头按键
@end


#define WIDTH [UIScreen mainScreen].bounds.size.width
#define HEIGHT [UIScreen mainScreen].bounds.size.height

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor blackColor];
    //初始化Camrec对象 GPUImageOutput 设置默认后置摄像头 取得图像大小等属性
    videoCamera = [[GPUImageVideoCamera alloc] initWithSessionPreset:AVCaptureSessionPreset640x480 cameraPosition:AVCaptureDevicePositionBack];
    videoCamera.outputImageOrientation = UIInterfaceOrientationPortrait;
    //关闭前后摄像头镜像显示
    videoCamera.horizontallyMirrorFrontFacingCamera = NO;
    videoCamera.horizontallyMirrorRearFacingCamera = NO;
    
    //初始化input对象
    filter = [[GPUImageFilter alloc] init];
    //如果我没猜错的话,这个应该是预览图层
    GPUImageView *filterView = [[GPUImageView alloc] initWithFrame:CGRectMake(0, 80, WIDTH, HEIGHT - 150)];
    //加入主视图
    [self.view addSubview:filterView];
    //拼出写入路径
    self.pathToMovie = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/Movie.m4v"];
    unlink([self.pathToMovie UTF8String]);
    self.movieURL = [NSURL fileURLWithPath:self.pathToMovie];
    //文件写入对象
    movieWriter = [[GPUImageMovieWriter alloc] initWithMovieURL:self.movieURL size:CGSizeMake(480.0, 640.0)];
    movieWriter.encodingLiveVideo = YES;
    //分别加入会话
    [videoCamera addTarget:filter];
    
    [filter addTarget:filterView];
    
    [filter addTarget:movieWriter];
    
    [videoCamera startCameraCapture];
    
    //创建录制开关按钮
    UIButton *StarBtn = [[UIButton alloc] initWithFrame:CGRectMake((WIDTH - 50)/2.0, HEIGHT - 50, 50, 50)];
    StarBtn.backgroundColor = [UIColor purpleColor];
    [StarBtn setTitle:@"star" forState:UIControlStateNormal];
    [self.view addSubview:StarBtn];
    [StarBtn addTarget:self action:@selector(clicBtn:) forControlEvents:UIControlEventTouchUpInside];
    
    //添加闪光灯开关
    UISwitch *ch = [[UISwitch alloc] initWithFrame:CGRectMake(WIDTH - 60, HEIGHT - 55, 50, 30)];
    [self.view addSubview:ch];
    [ch setOnTintColor:[UIColor greenColor]];
    [ch addTarget:self action:@selector(turnTorchOn:) forControlEvents:UIControlEventValueChanged];
    ch.on = NO;
    
    //创建摄像头切换开关
    _changeCamre = [UIButton buttonWithType:UIButtonTypeCustom];
    _changeCamre.frame = CGRectMake(CGRectGetMinX(ch.frame) - 84, HEIGHT - 74, 74, 74);
    [self.view addSubview:_changeCamre];
    [_changeCamre setImage:[UIImage imageNamed:@"btn_video_flip_camera"] forState:UIControlStateNormal];
    [_changeCamre addTarget:self action:@selector(toggleButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    
    //图层色值条
    self.slider = [[UISlider alloc] initWithFrame:CGRectMake(20, 20, WIDTH - 40, 30)];
    [self.view addSubview:self.slider];
    [self.slider addTarget:self action:@selector(MoveSliderEvent:) forControlEvents:UIControlEventValueChanged];
    
}
//摄像头切换
- (void)toggleButtonClick:(UIButton *)btn
{
    
    [videoCamera rotateCamera];
    
}
//调整色值
- (void)MoveSliderEvent:(UISlider *)slider
{
    [(GPUImageSepiaFilter *)filter setIntensity:[slider value]];
}
- (void)clicBtn:(UIButton *)btn
{
    if(!self.coding)
    {
        [movieWriter startRecording];
        [btn setTitle:@"stop" forState:UIControlStateNormal];
    }else
    {
        [btn setTitle:@"star" forState:UIControlStateNormal];
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提示" message:@"保存视频" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *action = [UIAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            //视频录入完成之后在后台将视频存储到相簿
            
            [movieWriter finishRecording];
            ALAssetsLibrary *assetsLibrary=[[ALAssetsLibrary alloc] init];
            [assetsLibrary writeVideoAtPathToSavedPhotosAlbum:self.movieURL completionBlock:^(NSURL *assetURL, NSError *error) {
                if (error) {
                    
                    [MBProgressHUD showError:[NSString stringWithFormat:@"保存视频到相簿过程中发生错误，错误信息：%@",error.localizedDescription]];
                }
                NSLog(@"outputUrl:%@",self.movieURL);
                [[NSFileManager defaultManager] removeItemAtURL:self.movieURL error:nil];
                [MBProgressHUD showSuccess:@"成功保存视频到相簿."];
                [self dismissViewControllerAnimated:YES completion:nil];
                
            }];

            
        }];
        UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            [btn setTitle:@"star" forState:UIControlStateNormal];
            [movieWriter cancelRecording];
            [MBProgressHUD showSuccess:@"取成功."];
            [self dismissViewControllerAnimated:YES completion:nil];
        }];
        
        [alert addAction:action];
        [alert addAction:action1];
        [self presentViewController:alert animated:YES completion:nil];
        
        
    }
                [videoCamera.inputCamera lockForConfiguration:nil];
                [videoCamera.inputCamera setTorchMode:AVCaptureTorchModeOff];
                [videoCamera.inputCamera unlockForConfiguration];

    self.coding = !self.coding;
}
//闪光灯开关
- (void) turnTorchOn:(UISwitch *)ch{
    
    Class captureDeviceClass = NSClassFromString(@"AVCaptureDevice");
    if (captureDeviceClass != nil) {
        AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
        if ([device hasTorch] && [device hasFlash]){
            
            [device lockForConfiguration:nil];
            if (ch.isOn) {
                [device setTorchMode:AVCaptureTorchModeOn];
                [device setFlashMode:AVCaptureFlashModeOn];
                
            } else {
                [device setTorchMode:AVCaptureTorchModeOff];
                [device setFlashMode:AVCaptureFlashModeOff];
                
            }
            [device unlockForConfiguration];
        }
    }
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    // Map UIDeviceOrientation to UIInterfaceOrientation.
    UIInterfaceOrientation orient = UIInterfaceOrientationPortrait;
    switch ([[UIDevice currentDevice] orientation])
    {
        case UIDeviceOrientationLandscapeLeft:
            orient = UIInterfaceOrientationLandscapeLeft;
            break;
            
        case UIDeviceOrientationLandscapeRight:
            orient = UIInterfaceOrientationLandscapeRight;
            break;
            
        case UIDeviceOrientationPortrait:
            orient = UIInterfaceOrientationPortrait;
            break;
            
        case UIDeviceOrientationPortraitUpsideDown:
            orient = UIInterfaceOrientationPortraitUpsideDown;
            break;
            
        case UIDeviceOrientationFaceUp:
        case UIDeviceOrientationFaceDown:
        case UIDeviceOrientationUnknown:
            // When in doubt, stay the same.
            orient = fromInterfaceOrientation;
            break;
    }
    videoCamera.outputImageOrientation = orient;
    
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES; // Support all orientations.
}
@end
